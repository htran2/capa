# CAPACITOR MOBILE APP MAKING

Five folders here are full projects, ordered by the "steps" of an app's hybrid development. 

**Goal**: to make an app for all platforms: web, Android and iOS with _one_ codebase. 

**Tool**: [Capacitor](https://capacitorjs.com) and knowledge of your webapp dev skills (HTML, CSS, React, etc.)

Each folder is a project, you `cd` to it, run `npm install` then `npm start`, `npm run build` etc. See its instructions inside.


- 01- An empty webapp
- 02- Adding routing to empty webapp in 01
- 03- Using capacitor to create Android and iOS projects. Then use IDEs to open them, build and deploy to mobile devices.
- 04- From 01, capacitorise it as in 03, then add a few mobile plugins
- 05- An attempt to see how nice Capacitor can help us make mobile apps so quickly and easily from a complex webapp


To set up for hybrid development this way you shall need softwares and tools, something like,
- Java, certification 
- Gradle
- Android Studio
- Xcode
- etc.

Find somebody in Bizbank to assist you for specifics. I don't write them all here because these things change over time.