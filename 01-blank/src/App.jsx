import pic from './assets/squid.png'
import './App.css'

const App = () => {

  return (
    <>
      <h1>VITE WEBAPP</h1>
      <img width="200" src={pic} />
    </>
  )
}

export default App
