import { Link } from "react-router-dom"
import duck from './assets/duck.avif'

const Duck = () => {

  return (
    <>
      <h1>DUCK</h1>
      <img width="300" src={duck} />
      <br />
      <Link to={`/`}>Home</Link>
    </>
  )
}

export default Duck
