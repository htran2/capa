import { Link } from "react-router-dom"
import './Home.scss'

const Home = () => {

  return (
    <>
      <h1>HOME WITH ROUTES</h1>
      <Link to={`/duck`}>Duck</Link>
      <Link to={`/chick`}>Chick</Link>
      <Link to={`/kit/3`}>3 Kitties</Link>
    </>
  )
}

export default Home
