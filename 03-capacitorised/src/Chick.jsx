import { Link } from "react-router-dom"
import kit from './assets/chick.avif'

const Chick = () => {

  return (
    <>
      <h1>CHICK</h1>
      <img width="300" src={kit} />
      <br />
      <Link to={`/`}>Home</Link>
    </>
  )
}

export default Chick
