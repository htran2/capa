# App with routing, then capacitorized

Copied from `02-routed` project

Created `dist` folder

Changed `name` and `version` in `package.json`

Followed https://capacitorjs.com/docs/getting-started

```
npm i @capacitor/core
npm i --save-dev @capacitor/cli
npx cap init
   // Answers: animal, com.atb.animal

npm i @capacitor/android @capacitor/ios

npm run build

npx cap add android
npx cap add ios

npx cap sync
```

At this time the Android and iOS **projects** were created. Let's open them in IDE.

```
npx cap open android
```
or
```
npx cap open ios
```

In the IDE, build the mobile app and deploy it on simulator or your phone (connected to laptop with a USB cable).


## ICON, SPLASH
To use your own app logo, splash screens:
```
npm install @capacitor/assets --save-dev
```
Get images in `resources` folder, rename them by this [guide](https://capacitorjs.com/docs/guides/splash-screens-and-icons)

```
npx capacitor-assets generate
npx cap sync
```
Redeploy. Your app should have the correct icon and splash screen.

Let's try to add mobile-only feature in project 04.
