import { useEffect, useState, useLayoutEffect, useRef } from 'react'
import Header from './Header'
import { arc_diagram_data } from './d3-data'


// WIP
// MAKE THIS A HOOK




// A reponsive container for D3 diagram
const D3Responsive = ({ headerText, children }) => {
  const [ width, setWidth ] = useState()
  const [ height, setHeight ] = useState()
  const surface = useRef() // DIV holding diagram, responsive
  const handleResize = () => {
    // find the new size, then update chart's size
    const rect = surface.current.getBoundingClientRect()
    setWidth(rect.width)
    setHeight(rect.width * 2 / 3)
  }
  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  useLayoutEffect(handleResize, []);    
  return (
    <>
      <Header text="Responsive page" />
      <div id="surface" ref={surface}>
        <ArcDiagram width={width} height={height} data={arc_diagram_data} />
      </div>
    </>
  )
}

export default D3Responsive
