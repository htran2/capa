export default `
---
title: Remote Deposit Capture by INTRIA 
---
sequenceDiagram
    actor M as MOBILE USER
    participant E as ATB ENT
    participant S as ATB SAP
    participant I as INTRIA

    Note over M: START - Remore Deposit page
    M-)+E: GET /depositAccounts 
    E-)+S: request all deposit accounts
    S--)-E: response all deposit accounts
    E--)-M: response 200

    M->>M: UI data input:<br/>account, amount, cheque photos

    M-)+E: POST /chequeimages

    destroy E


`