import { useEffect, useState, useLayoutEffect, useRef } from 'react'

// given a container element, like a ref on a DIV,
// 'responsively' returns width and height of the container
const useResponsiveContainer = (containerRef) => {
  const [ width, setWidth ] = useState(300)
  const [ height, setHeight ] = useState(200)
  const handleResize = () => {
    // find the new size, then update chart's size
    const rect = containerRef.current.getBoundingClientRect()
    setWidth(rect.width)
    setHeight(rect.width * 2 / 3)
  }
  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  useLayoutEffect(handleResize, []);    
  
  return { width, height }
}

export default useResponsiveContainer
