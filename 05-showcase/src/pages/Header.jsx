import { Link } from "react-router-dom"
import home from '../assets/home.jpg'

const Header = ({ text }) => {

  return (
    <div style={{ height: '4rem' }}>
      <div style={{ float: 'left', width: '70%' }}>
        <h1>{text}</h1>
      </div>
      <div style={{ float: 'right' }}>
        <Link to={`/`}><img height={40} src={home} /></Link>
      </div>
    </div>
  )
}

export default Header
