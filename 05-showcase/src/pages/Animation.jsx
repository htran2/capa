import { useEffect } from 'react'
import { Link } from "react-router-dom"
import Header from './Header'

// Try animation from gsap.com 
const Animation = () => {
  useEffect(() => {
    const canvas = document.getElementById('canvas')
    canvas.style.display = 'inherit'
    window.dispatchEvent(new Event('resize')) // hack
    return () => {
      canvas.style.display = 'none'
    }
  }, [])
  return (
    <>
      <Header text="Animation (GSAP library)" />
    </>
  )
}

export default Animation
