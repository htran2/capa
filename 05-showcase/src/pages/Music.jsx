import SheetMusic from '@slnsw/react-sheet-music'
import Header from './Header'


const music_Sample = `
X: 1
T: 简单操作
M: 4/4
L: 1/8
K: Emin
|:ABCD|EFGA|abcd|efga|
|A2B2C2D2|E2F2G2A2|a2b2c2d2|e2f2g2a2||`


const Music = () => {

  return (
    <>
      <Header text="Music" />
      <div className="music">
        <SheetMusic
          // Textual representation of music in ABC notation
          // The string below will show four crotchets in one bar
          notation={music_Sample}
        />
        <h2>Source in ABC notation</h2>
        <pre>
          {music_Sample}
        </pre>
      </div>
    </>
  )
}

export default Music
