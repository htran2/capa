import { useRef, useState, useEffect } from 'react'
import * as d3 from "d3"
import Header from './Header'
import useResponsiveContainer from './useResponsiveContainer'

// Learned from https://www.react-graph-gallery.com/arc-diagram


// Return the d attribute of a SVG path element for an arc
// that joins 2 points horizontally using an Elliptical Arc Curve
const horizontalArc = (xStart, yStart, xEnd, yEnd) => {
  return [
    // arc starts at xStart, xEnd
    "M", xStart, yStart,
    // (A)rc Curve https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d#elliptical_arc_curve
    "A",
    (xStart - xEnd) / 2, // rx: first radii of the ellipse (inflexion point)
    (xStart - xEnd) / 2, // ry: second radii of the ellipse  (inflexion point)
    0, // angle: rotation (in degrees) of the ellipse relative to the x-axis
    1, // large-arc-flag: large arc (1) or small arc (0)
    xStart < xEnd ? 1 : 0, // sweep-flag: the clockwise turning arc (1) or counterclockwise turning arc (0)
    // end of the arc
    xEnd, ",", yEnd,
  ].join(" ");
};

const COLORS = ["gold", "salmon", "goldenrod", "aqua" ];
const MARGIN = { top: 20, right: 150, bottom: 150, left: 50 };

const ArcDiagram = ({ data }) => {
  const containerRef = useRef()
  const { width, height } = useResponsiveContainer(containerRef)
  const boundsWidth = width - MARGIN.right - MARGIN.left;
  const boundsHeight = height - MARGIN.top - MARGIN.bottom;
  
  // Compute the nodes, each will be represented by a small coloured circle
  const allNodeNames = data.nodes.map(node => node.id);
  const allNodeGroups = [...new Set(data.nodes.map((node) => node.group))];
  const xScale = d3.scalePoint().range([0, boundsWidth]).domain(allNodeNames);
  const colorScale = d3.scaleOrdinal().domain(allNodeGroups).range(COLORS);
  const allNodes = data.nodes.map(node => {
    const radius = 6
    return (
      <circle key={node.id} r={radius}
        cx={xScale(node.id)} cy={boundsHeight}
        fill={colorScale(node.group)}
        onMouseOver={e => e.target.setAttribute('r', radius * 1.6)}
        onMouseOut={e => e.target.setAttribute('r', radius)}
      />
    );
  });

  // Compute the node labels
  const allLabels = data.nodes.map(node => {
    return (
      <text key={node.id} 
        x={xScale(node.id)} y={boundsHeight}
        transform={`rotate(60 ${xScale(node.id)} ${boundsHeight}) translate(20, 5)`}
      >
        {node.id}
      </text>
    );
  });
  // Compute the links, each is represented by an elliptical arc
  const allLinks = data.links.map((link, i) => {
    const xStart = xScale(link.source);
    const xEnd = xScale(link.target);
    if (typeof xStart === "undefined" || typeof xEnd === "undefined") {
      return;
    }
    return (
      <path key={i} stroke="black" fill="none"
        d={horizontalArc(xStart, boundsHeight, xEnd, boundsHeight)}
      />
    );
  });
  return (
    <div id="arc-diagram" ref={containerRef}>
      <svg width={width} height={height}>
        <g width={boundsWidth} height={boundsHeight}
          transform={`translate(${[MARGIN.left, MARGIN.top].join(",")})`}
        >
          {allNodes} {allLabels} {allLinks}
        </g>
      </svg>
    </div>
  );
}


const D3ArcDiagram = () => {
  const [ data, setData ] = useState() 
  useEffect(() => {
    const fetchData = async () => {
      const contents = await d3.json('data-arc-diagram-small.json')
      setData(contents)
      console.log(contents)
    }
    fetchData()
  }, [])
  return (data && 
    <>
       <Header text="D3 Arc diagram" />
       <ArcDiagram data={data} />
    </>
  )
}

export default D3ArcDiagram

