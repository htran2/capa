import { Link } from "react-router-dom"
import home from '../assets/home.jpg'

const Doodle = () => {

  return (
    <>
      <Link to={`/`}><img height={50} src={home} /></Link>
      <h1>Doodle</h1>
     
    </>
  )
}

export default Doodle
