import { useEffect } from 'react'
import mermaid from "mermaid"
import Header from './Header'
import config from './mermaid-config'
import data from './mermaid-data'

// Mermaid chart component, accepting "mermaid" data prop

mermaid.initialize(config);

const Mermaid = () => {

  useEffect(() => {
    mermaid.contentLoaded()
  }, [])

  return (
    <>
      <Header text="Diagrams (Mermaid library)" />
      <div className="mermaid">{data}</div>
    </>
  )
}

export default Mermaid
