import { Link } from "react-router-dom"
import { Capacitor } from '@capacitor/core'
import './Home.scss'

const Home = () => {

  return (
    <>
      <h1>SHOWCASE CAPACITOR</h1>
      <Link to={`/responsive`}>Responsive</Link>
      <Link to={`/animation`}>Animation</Link>
      <Link to={`/diagram`}>Mermaid diagram</Link>
      <Link to={`/d3-arc`}>D3 arc diagram</Link>
      <Link to={`/d3-circle`}>D3 circle packing diagram</Link>
      {!Capacitor.isNativePlatform() && <Link to={`/music`}>Music editor</Link>}
    </>
  )
}

export default Home
