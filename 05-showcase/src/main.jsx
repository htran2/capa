import React from 'react'
import { createBrowserRouter, RouterProvider } from "react-router-dom"
import ReactDOM from 'react-dom/client'
import Home from './Home'
import Mermaid from './pages/Mermaid'
import D3ArcDiagram from './pages/D3ArcDiagram'
import D3CirclePacking from './pages/D3CirclePacking'
import Animation from './pages/Animation'
import Music from './pages/Music'
import Kit from './Kit'
import Responsive from './pages/Responsive'

const router = createBrowserRouter([
  { path: "/", element: <Home /> },
  { path: "/responsive", element: <Responsive /> },
  { path: "/animation", element: <Animation /> },
  { path: "/diagram", element: <Mermaid /> },
  { path: "/d3-arc", element: <D3ArcDiagram /> },
  { path: "/d3-circle", element: <D3CirclePacking /> },
  { path: "/music", element: <Music /> },
  { path: "/kit/:n", element: <Kit /> },
])


ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router}>
    <Home />
  </RouterProvider>
)
