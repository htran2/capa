# App showing a few web's possiblities that can live in all platforms

Copy from `03-capacitorised`

Change `name` and `version` in package.json

```
npm install
npx cap init
   Answers: showcase, com.atb.showcase
mkdir dist
npm run build

npx cap add android
npx cap add ios
```

## Change logo
Put your images in `resources` folder

npx capacitor-assets generate
npx cap sync

## IDE
```
npx cap open android
npx cap open ios
```

Build the app and test.

