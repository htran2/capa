import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.atb.showcase',
  appName: 'showcase',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;
