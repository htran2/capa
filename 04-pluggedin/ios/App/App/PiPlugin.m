//
//  Step 2: Create this file in Xcode
//  App/App --> New file --> Objective-C --> Create Bridging Header
//    --> edit contents
//

#import <Capacitor/Capacitor.h>

// Define the plugin using the CAP_PLUGIN Macro, and
// each method the plugin supports using the CAP_PLUGIN_METHOD macro.
CAP_PLUGIN(PiPlugin, "Pi",
    CAP_PLUGIN_METHOD(getValue, CAPPluginReturnPromise);
);
