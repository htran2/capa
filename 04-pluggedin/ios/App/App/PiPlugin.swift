// In Xcode, follow https://capacitorjs.com/docs/ios/custom-code
// Don't write these files in text editor
// Step 1: App/App --> New file --> Swift File --> edit contents

import Capacitor

@objc(PiPlugin)
public class PiPlugin: CAPPlugin {
  @objc func getValue(_ call: CAPPluginCall) {
    call.resolve([
        "value": "3.141666 (= 377/120, Ptolemy, 150 AD)",
        "source": "PiPlugin.swift, an iOS Capacitor plugin"
    ])
  }
}
