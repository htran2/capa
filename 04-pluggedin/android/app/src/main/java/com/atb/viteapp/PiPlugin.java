package com.atb.viteapp;

import com.getcapacitor.*;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "Pi")
public class PiPlugin extends Plugin {
    @PluginMethod
    public void getValue(PluginCall pluginCall) {
        JSObject pluginResult = new JSObject();
        pluginResult.put("value", Math.PI);
        pluginResult.put("source", "PiPlugin.java, an Android Capacitor plugin");
        pluginCall.resolve(pluginResult);
    }
}
