import React from 'react'
import ReactDOM from 'react-dom/client'
import { Capacitor } from '@capacitor/core';
import HomeMobile from './HomeMobile'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    {Capacitor.isNativePlatform() ? <HomeMobile /> : <h1>This app is mobile-only!</h1>}
  </React.StrictMode>,
)
