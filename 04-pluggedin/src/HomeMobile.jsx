import { useEffect, useState } from 'react'
import './HomeMobile.scss'
import {CapacitorFlash as F} from '@capgo/capacitor-flash'
import { Haptics, ImpactStyle } from '@capacitor/haptics'
import { registerPlugin } from '@capacitor/core';

const PiPlugin = registerPlugin('Pi');

const HomeMobile = () => {
  const [ isOn, setOn ] = useState(false)
  const [ pi, setPi ] = useState()

  const flick = async () => {
    if (isOn) {
      F.switchOff()
      setOn(false)
      await Haptics.impact({ style: ImpactStyle.Light });
    } else {
      F.switchOn()
      setOn(true)
      await Haptics.impact({ style: ImpactStyle.Medium });
    }
  }

  useEffect(async () => {
    const v = await PiPlugin.getValue()
    setPi(v ? v.value : 'unknown')
  }, [])

  return (
    <>
      <h1>TORCH</h1>
      <button onClick={flick}>{(isOn ? 'LIGHT OFF' : 'LIGHT ON')}</button>
      
      <h1>VIBRATION</h1>      
      <button onClick={async () => await Haptics.vibrate()}>
        VIBRATE ME
      </button>



      <hr />
      
      
      <h2>From "Pi" plugin</h2>
      <h3>{pi}</h3>
    </>
  )
}

export default HomeMobile
