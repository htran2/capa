# App mobile-only, with Capacitor plugins

Copied from `01-blank` project
Capacitorised it (it's your job)
Prepare your own icons in `/resources`

```
npm install
npx capacitor-assets generate
mkdir dist
npm run build
npx cap sync
```

I added a couple of plugins. You do it too.

Browse the library of public plugins `https://capacitorjs.com/directory`

Choose one, add it in. For example I did this:
```
npm install @capgo/capacitor-flash
npx cap sync
```

Deploy on device. No webapp here.

Our mobile app can access the torch now. Read the [docs](https://github.com/Cap-go/capacitor-flash)

Write code to turn it on/off.

Try another plugin, to vibrate the phone.

Make another plugin, this time it's our own, named it "Pi."  It returns the value of π

See `PiPlugin.java` and `MainActivity.java`

See `PiPlugin.swift` and `PiPlugin.m`


You reach this far. It measn you begin to know what Capacitor is.

Next project `05` is just an exercise to see whether some fancies in webapp, such as cool CSS, animations, SVG,... can survive well in mobile world. 



